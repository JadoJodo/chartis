package status

import "testing"

func TestAddStatus(t *testing.T) {
	t.Run("should add a Status to the Statuses", func(t *testing.T) {
		statuses := Statuses{}
		status := Status{name: "To Do", position: 1}

		statuses.AddStatus(status)

		want := 1
		got := len(statuses.statuses)

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkAddStatus(b *testing.B) {
	statuses := Statuses{}
	status := Status{}
	b.Run("add a Status", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			statuses.AddStatus(status)
		}
	})
}
func TestRemoveStatus(t *testing.T) {
	t.Run("should remove a Status from the Statuses", func(t *testing.T) {
		statuses := Statuses{}
		status := Status{}
		statuses.statuses = append(statuses.statuses, status)

		statuses.RemoveStatus(status)

		want := 0
		got := len(statuses.statuses)

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkRemoveStatus(b *testing.B) {
	status := Status{}
	statuses := Statuses{}

	for i := 0; i < b.N; i++ {
		statuses.statuses = append(statuses.statuses, status)
	}

	b.Run("remove a Status", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			statuses.RemoveStatus(status)
		}
	})
}
func TestName(t *testing.T) {
	t.Run("should return the name of a Status", func(t *testing.T) {
		status := Status{name: "In Progress"}

		want := "In Progress"
		got := status.Name()

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkName(b *testing.B) {
	status := Status{name: "Test"}
	b.Run("return the Task name", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			status.Name()
		}
	})
}
func TestSetName(t *testing.T) {
	t.Run("should set a name on a Status", func(t *testing.T) {
		status := Status{}
		status.SetName("In Progress")

		want := "In Progress"
		got := status.name

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkSetName(b *testing.B) {
	status := Status{}
	b.Run("set the Status name", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			status.SetName("Test")
		}
	})
}
func TestPosition(t *testing.T) {
	t.Run("should get the position of a Status", func(t *testing.T) {
		status := Status{position: 1}

		want := 1
		got := status.Position()

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkPosition(b *testing.B) {
	status := Status{position: 1}
	b.Run("return the Status position", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			status.Position()
		}
	})
}
func TestSetPosition(t *testing.T) {
	t.Run("should set the position on a Status", func(t *testing.T) {
		status := Status{}
		status.SetPosition(1)

		want := 1
		got := status.position

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkSetPosition(b *testing.B) {
	status := Status{}
	b.Run("set the Status position", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			status.SetPosition(1)
		}
	})
}
func TestCreateStatus(t *testing.T) {
	t.Run("should create a new Status", func(t *testing.T) {
		want := Status{name: "In Progress", position: 1}
		got := CreateStatus("In Progress")

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}

func BenchmarkCreateStatus(b *testing.B) {
	b.Run("benchmark CreateStatus", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			CreateStatus("In Progress")
		}
	})

}
func TestCreateDefaultStatuses(t *testing.T) {
	t.Run("should have a length of 1", func(t *testing.T) {
		statuses := createDefaultStatuses()
		want := 1
		got := len(statuses)

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
	t.Run("should create the default Statuses", func(t *testing.T) {
		statuses := createDefaultStatuses()

		want := Status{name: "To Do", position: 1}
		got := statuses[0]

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkCreateDefaultStatuses(b *testing.B) {
	b.Run("benchmark CreateDefaultStatuses", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			createDefaultStatuses()
		}
	})

}
