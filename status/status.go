package status

// Statuses contains multiple status instances
type Statuses struct {
	statuses []Status
}

// AddStatus appends a Status to the existing Statuses
func (s *Statuses) AddStatus(status Status) {
	s.statuses = append(s.statuses, status)
}

// RemoveStatus removes a given Status from the existing Statuses
func (s *Statuses) RemoveStatus(statusToRemove Status) {
	for i, status := range s.statuses {
		if status == statusToRemove {
			s.statuses = append(s.statuses[:i], s.statuses[i+1:]...)
		}
	}
}

// Status allows Tasks to be grouped and separated
type Status struct {
	name     string
	position int
}

// Name returns a Status name
func (s *Status) Name() string {
	return s.name
}

// SetName adds the name of a Status
func (s *Status) SetName(name string) {
	s.name = name
}

// Position returns the position of a Status
func (s *Status) Position() int {
	return s.position
}

// SetPosition sets the position of a Status
func (s *Status) SetPosition(position int) {
	s.position = position
}

// CreateStatus creates a Status
func CreateStatus(name string) (s Status) {
	s = Status{}

	s.SetName(name)
	s.SetPosition(len(statuses.statuses) + 1)

	return
}

func createDefaultStatuses() []Status {
	return []Status{
		CreateStatus(defaultStatus),
	}
}

const defaultStatus string = "To Do"

var statuses Statuses
