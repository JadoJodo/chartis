package task

import (
	status "gitlab.com/jadojodo/chartis/status"

	uuid "github.com/satori/go.uuid"
)

// Tasks contains multiple Task instances
type Tasks struct {
	tasks []Task
}

// AddTask appends a Task to the existing Tasks
func (t *Tasks) AddTask(task Task) {
	t.tasks = append(t.tasks, task)
}

// RemoveTask removes a given Task from the existing Tasks
func (t *Tasks) RemoveTask(taskToRemove Task) {
	for i, task := range t.tasks {
		if task == taskToRemove {
			t.tasks = append(t.tasks[:i], t.tasks[i+1:]...)
		}
	}
}

// Task represents a unit of work.
type Task struct {
	id                 uuid.UUID
	title, description string
	status             status.Status
}

// ID returns a Task's ID
func (t Task) ID() uuid.UUID {
	return t.id
}

// SetID sets a Task's ID
func (t *Task) SetID(id uuid.UUID) {
	t.id = id
}

// Title returns the Task title
func (t Task) Title() string {
	return t.title
}

// SetTitle sets a Task title
func (t *Task) SetTitle(title string) {
	t.title = title
}

// Description returns the Task description
func (t Task) Description() string {
	return t.description
}

// SetDescription sets a Task description
func (t *Task) SetDescription(description string) {
	t.description = description
}

// Status returns the Task status
func (t Task) Status() status.Status {
	return t.status
}

// SetStatus sets a Task status
func (t *Task) SetStatus(status status.Status) {
	t.status = status
}

// CreateTask creates and returns a new Task
func CreateTask(title string, description string) (t Task) {
	t = Task{}

	t.SetTitle(title)
	t.SetDescription(description)

	return
}
