package task

import (
	"testing"

	status "gitlab.com/jadojodo/chartis/status"

	uuid "github.com/satori/go.uuid"
)

func TestID(t *testing.T) {
	t.Run("should return the Task ID", func(t *testing.T) {
		id, _ := uuid.NewV4()
		task := Task{id: id}

		want := id
		got := task.ID()

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkID(b *testing.B) {
	id, _ := uuid.NewV4()
	task := Task{id: id}
	b.Run("return the Task ID", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.ID()
		}
	})
}
func TestSetID(t *testing.T) {
	t.Run("should set the Task ID", func(t *testing.T) {
		id, _ := uuid.NewV4()
		task := Task{id: id}

		task.SetID(id)

		want := id
		got := task.id

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkSetID(b *testing.B) {
	id, _ := uuid.NewV4()
	task := Task{}
	b.Run("set a new Task ID", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.SetID(id)
		}
	})
}
func TestTitle(t *testing.T) {
	t.Run("should return the Task Title", func(t *testing.T) {
		task := Task{title: "Do the dishes"}

		want := "Do the dishes"
		got := task.Title()

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkTitle(b *testing.B) {
	task := Task{title: "Test"}
	b.Run("return the Task Title", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.Title()
		}
	})
}
func TestSetTitle(t *testing.T) {
	t.Run("should set the Task title", func(t *testing.T) {
		task := Task{}

		task.SetTitle("Do the dishes")

		want := "Do the dishes"
		got := task.title

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkSetTitle(b *testing.B) {
	task := Task{}
	b.Run("set the Task title", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.SetTitle("Test")
		}
	})
}
func TestDescription(t *testing.T) {
	t.Run("should return the Task description", func(t *testing.T) {
		task := Task{description: "The bowls and plates are dirty and need to be cleaned."}

		want := "The bowls and plates are dirty and need to be cleaned."
		got := task.Description()

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkDescription(b *testing.B) {
	task := Task{description: "Test"}
	b.Run("return the Task description", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.Description()
		}
	})
}
func TestSetDescription(t *testing.T) {
	t.Run("should set the Task description", func(t *testing.T) {
		task := Task{}

		task.SetDescription("The bowls and plates are dirty and need to be cleaned.")

		want := "The bowls and plates are dirty and need to be cleaned."
		got := task.description

		if got != want {
			t.Errorf("Expected '%s' got '%s'", want, got)
		}
	})
}
func BenchmarkSetDescription(b *testing.B) {
	task := Task{}
	b.Run("set the Task description", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.SetDescription("Test")
		}
	})
}
func TestTaskStatus(t *testing.T) {
	t.Run("should return the Task status", func(t *testing.T) {
		task := Task{status: status.CreateStatus("In Progress")}

		status := status.Status{}
		status.SetName("In Progress")
		status.SetPosition(1)

		want := status
		got := task.Status()

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkTaskStatus(b *testing.B) {
	status := status.Status{}
	task := Task{status: status}
	b.Run("return the Task Status", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.Status()
		}
	})
}
func TestTaskSetStatus(t *testing.T) {
	t.Run("should set the Task status", func(t *testing.T) {
		task := Task{}
		status := status.CreateStatus("In Progress")

		task.SetStatus(status)

		want := status
		got := task.status

		if got != want {
			t.Errorf("Expected '%v' got '%+v'", want, got)
		}
	})
}
func BenchmarkSetTaskStatus(b *testing.B) {
	task := Task{}
	status := status.Status{}
	b.Run("set the Task Status", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.SetStatus(status)
		}
	})
}
func TestStatus(t *testing.T) {
	t.Run("should return the Task Status", func(t *testing.T) {
		status := status.Status{}
		status.SetName("In Progress")
		task := Task{status: status}

		want := status
		got := task.Status()

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkStatus(b *testing.B) {
	status := status.Status{}
	task := Task{status: status}
	b.Run("return the Task Status", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			task.Status()
		}
	})
}
func TestCreateTask(t *testing.T) {
	t.Run("should create a new Task", func(t *testing.T) {
		want := Task{title: "Do the dishes", description: "The bowls and plates are dirty and need to be cleaned."}
		got := CreateTask("Do the dishes", "The bowls and plates are dirty and need to be cleaned.")

		if got != want {
			t.Errorf("Expected '%+v' got '%+v'", want, got)
		}
	})
}
func BenchmarkCreateTask(b *testing.B) {
	b.Run("create a Task", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			CreateTask("Do the dishes", "The bowls and plates are dirty and need to be cleaned.")
		}
	})
}
func TestAddTask(t *testing.T) {
	t.Run("should add a Task to the Tasks", func(t *testing.T) {
		tasks := Tasks{}
		task := Task{title: "Do the dishes", description: "The bowls and plates are dirty and need to be cleaned."}

		tasks.AddTask(task)

		want := 1
		got := len(tasks.tasks)

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkAddTask(b *testing.B) {
	tasks := Tasks{}
	task := Task{}
	b.Run("add a Task", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			tasks.AddTask(task)
		}
	})
}
func TestRemoveTask(t *testing.T) {
	t.Run("should remove a Task from the Tasks", func(t *testing.T) {
		tasks := Tasks{}
		task := Task{}
		tasks.tasks = append(tasks.tasks, task)

		tasks.RemoveTask(task)

		want := 0
		got := len(tasks.tasks)

		if got != want {
			t.Errorf("Expected '%d' got '%d'", want, got)
		}
	})
}
func BenchmarkRemoveTask(b *testing.B) {
	task := Task{}
	tasks := Tasks{}

	for i := 0; i < b.N; i++ {
		tasks.tasks = append(tasks.tasks, task)
	}

	b.Run("remove a Task", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			tasks.RemoveTask(task)
		}
	})
}
